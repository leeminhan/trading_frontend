export const environment = {
  production: true,
  baseUrl:
    "http://trade-backend-app-stock-trading-platform.singaporedevops7.conygre.com/",
  portfolioUrl:
    "http://portfolio-backend-app-stock-trading-platform.singaporedevops7.conygre.com/",
};
