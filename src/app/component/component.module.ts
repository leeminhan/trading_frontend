import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatCardModule } from "@angular/material/card";
import { MatPaginatorModule } from "@angular/material/paginator";
import { RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ChartsModule } from "ng2-charts";
import { ComponentsRoutes } from "./component.routing";
import { PortfolioComponent } from "./portfoliopage/portfoliopage.component";
import { TradingHistoryComponent } from "./tradinghistory/tradinghistory.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ComponentsRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    MatPaginatorModule,
    MatCardModule,
    ChartsModule,
  ],
  declarations: [TradingHistoryComponent, PortfolioComponent],
})
export class ComponentsModule {}
