import { Routes } from "@angular/router";
import { PortfolioComponent } from "./portfoliopage/portfoliopage.component";
import { TradingHistoryComponent } from "./tradinghistory/tradinghistory.component";

export const ComponentsRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "portfoliopage",
        component: PortfolioComponent,
        data: {
          title: "Portfolio",
          urls: [
            { title: "Dashboard", url: "/dashboard" },
            { title: "ngComponent" },
            { title: "Portfolio" },
          ],
        },
      },
      {
        path: "tradinghistory",
        component: TradingHistoryComponent,
        data: {
          title: "Trading History",
          urls: [
            { title: "Dashboard", url: "/dashboard" },
            { title: "ngComponent" },
            { title: "Trading History" },
          ],
        },
      },
    ],
  },
];
