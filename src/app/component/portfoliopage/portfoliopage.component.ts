import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { formatter } from "../../helper/helper-methods";
import { Portfolio } from "../../portfolio";
import { PortfolioService } from "../../portfolio.service";

@Component({
  templateUrl: "portfoliopage.component.html",
  styleUrls: ["portfoliopage.css"],
})
export class PortfolioComponent implements OnInit {
  public lineChartLegend = false;
  public lineChartType = "line";
  public dataArray = [];

  constructor(private portfolioService: PortfolioService) {}

  public checkboxGroupForm: FormGroup = Object.create(null);

  headers = [
    "ID",
    "Ticker",
    "Current Price",
    "Position",
    "Cost Basis",
    "Avg Price",
  ];

  donutTicker: string[] = [];
  donutQuantity: number[] = [];
  portfolioItem: Portfolio[] = [];
  portfolioAllPrice: Portfolio[] = [];
  portfolioTicker: string[] = [];
  ngOnInit(): void {
    this.portfolioService.getPortfolio().subscribe(
      (data: any) => {
        data.map((item: Portfolio) => {
          this.donutQuantity.push(parseFloat(item.costBasis));
          item.avgPrice = formatter.format(parseFloat(item.avgPrice));
          item.costBasis = formatter.format(parseFloat(item.costBasis));
          this.donutTicker.push(item.ticker);
          this.portfolioTicker.push(item.ticker);
        });
        this.portfolioItem = data;
      },
      (err: any) => console.log("Error")
    );
    this.portfolioService.getAllPrice().subscribe(
      (data: any) => {
        this.portfolioItem.map((item: Portfolio) => {
          const ticker = item.ticker;
          if (data[ticker])
            item.currPrice = formatter.format(parseFloat(data[ticker]));
        });
      },
      (err: any) => console.log("Error")
    );
  }

  // lineChart
  public lineChartData: Array<any> = [
    { data: [2.2, 4, 2.3, 3, 2.5, 4, 5, 5.2], label: "Portfolio Earnings" },
    {
      data: [2, 2.2, 2.4, 2.7, 2.65, 2.8, 2.9, 3.5],
      label: "Benchmark Standard",
    },
  ];

  public lineChartLabels: Array<string> = [
    "JAN",
    "FEB",
    "MAR",
    "APR",
    "MAY",
    "JUNE",
    "JUL",
    "AUG",
  ];

  public lineChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
  };

  public lineChartColors: Array<Object> = [
    {
      // red portfolio earning
      backgroundColor: "rgba(41, 98, 255,0.1)",
      borderColor: "rgb(146, 22, 22)",
      pointBackgroundColor: "rgb(146, 22, 22)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgb(146, 22, 22)",
    },
    {
      // dark blue bank earning
      backgroundColor: "rgba(116, 96, 238,0.1)",
      borderColor: "rgb(62, 71, 194)",
      pointBackgroundColor: "rgb(62, 71, 194)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgb(62, 71, 194)",
    },
  ];

  // DONUT
  public chartType: string = "doughnut";

  public chartDatasets: Array<any> = [
    { data: this.donutQuantity, label: "Portfolio Allocation" },
  ];

  public chartLabels: Array<any> = this.donutTicker;

  public legendPosition: any = {
    legend: { position: "right" },
  };

  public chartColors: Array<any> = [
    {
      backgroundColor: [
        "rgba(116, 96, 238,0.1)",
        "rgba(41, 98, 255,0.4)",
        "rgba(41, 98, 255, 0.4);",
        "#949FB1",
        "#4D5360",
      ],
      hoverBackgroundColor: [
        "rgba(116, 96, 238,0.3)",
        "rgba(41, 98, 255,0.6)",
        "rgba(41, 98, 255, 0.6);",
        "#949FB1",
        "#4D5360",
      ],
      borderWidth: 2,
    },
  ];

  public chartOptions: any = {
    responsive: true,
  };
  public chartClicked(e: any): void {}
  public chartHovered(e: any): void {}
}
