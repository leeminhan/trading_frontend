import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { formatter } from "../../helper/helper-methods";
import { TradeDetails } from "../../trade/trade-details/trade-details";
import { TradeService } from "../../trade/trade-details/trade-details.service";

@Component({
  templateUrl: "tradinghistory.component.html",
})
export class TradingHistoryComponent implements OnInit {
  constructor(private _tradeService: TradeService) {}
  sortedHistory() {
    const sortedValues = this.tradingHistoryItem.sort(
      (a: any, b: any) =>
        new Date(b.created).getTime() - new Date(a.created).getTime()
    );
    return sortedValues;
  }

  tradingHistoryItem: TradeDetails[] = [];

  public checkboxGroupForm: FormGroup = Object.create(null);

  public radioGroupForm: FormGroup = Object.create(null);

  headers = [
    "ID",
    "Created",
    "State",
    "Type",
    "Exchange",
    "Ticker",
    "Quantity",
    "Price (USD)",
  ];

  ngOnInit(): void {
    this._tradeService.getAllTrades().subscribe(
      (data: any) => {
        data.map((item: TradeDetails) => {
          item.price = formatter.format(parseFloat(item.price));
        });
        this.tradingHistoryItem = data;
      },
      (err: any) => console.log("Error")
    );
  }
}
