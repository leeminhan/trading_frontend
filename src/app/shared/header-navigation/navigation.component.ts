import { Component, EventEmitter, Output } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
//declare var $: any;

@Component({
  selector: "app-navigation",
  templateUrl: "./navigation.component.html",
})
export class NavigationComponent {
  @Output()
  toggleSidebar = new EventEmitter<void>();

  public showSearch = false;

  searchForm = new FormGroup({
    ticker: new FormControl(""),
  });

  constructor(private router: Router) {}

  onSubmit = () => {
    let ticker: string = this.searchForm.value.ticker;
    if (ticker.includes(":")) ticker = ticker.replace(":", "/");
    this.router.navigateByUrl(`/trade/${ticker}`).then(() => {
      window.location.reload();
    });
  };
}
