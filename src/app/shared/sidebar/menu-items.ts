import { RouteInfo } from "./sidebar.metadata";

export const ROUTES: RouteInfo[] = [
  {
    path: "",
    title: "Personal",
    icon: "mdi mdi-dots-horizontal",
    class: "nav-small-cap",
    extralink: true,
    submenu: [],
  },
  {
    path: "/dashboard",
    title: "Dashboard",
    icon: "mdi mdi-gauge",
    class: "",
    extralink: false,
    submenu: [],
  },
  {
    path: "/component/portfoliopage",
    title: "Portfolio",
    icon: "mdi mdi-blur-radial",
    class: "",
    extralink: false,
    submenu: [],
  },

  {
    path: "",
    title: "UI Components",
    icon: "mdi mdi-dots-horizontal",
    class: "nav-small-cap",
    extralink: true,
    submenu: [],
  },
  {
    path: "/trade/nasdaq/aapl",
    title: "Trade",
    icon: "mdi mdi-message-bulleted",
    class: "",
    extralink: false,
    submenu: [],
  },
  {
    path: "/component/tradinghistory",
    title: "Trading History",
    icon: "mdi mdi-calendar-clock",
    class: "",
    extralink: false,
    submenu: [],
  },
];
