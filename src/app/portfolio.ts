export class Portfolio {
  constructor(
    public id: number,
    public ticker: string,
    public quantity: number,
    public avgPrice: string,
    public costBasis: string,
    public currPrice?: string
  ) {}
}
