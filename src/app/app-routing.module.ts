import { Routes } from "@angular/router";
import { FullComponent } from "./layouts/full/full.component";
import { TradeComponent } from "./trade/trade.component";

export const Approutes: Routes = [
  {
    path: "",
    component: FullComponent,
    children: [
      { path: "", redirectTo: "/dashboard", pathMatch: "full" },
      {
        path: "dashboard",
        loadChildren: () =>
          import("./dashboard/dashboard.module").then((m) => m.DashboardModule),
      },
      {
        path: "component",
        loadChildren: () =>
          import("./component/component.module").then(
            (m) => m.ComponentsModule
          ),
      },
      {
        path: "trade/:tradeExchange/:ticker",
        component: TradeComponent,
        data: { title: "Stock Trading Page", backlink: undefined },
      },
    ],
  },
  {
    path: "**",
    redirectTo: "/dashboard",
  },
];
