import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { ChartsModule } from "ng2-charts";
import { MarketNewsComponent } from "./dashboard-components/market-news/market-news.component";
import { MarketWatchComponent } from "./dashboard-components/market-watch/market-watch.component";
import { TickerTapeComponent } from "./dashboard-components/ticker-tape/ticker-tape.component";
import { DashboardComponent } from "./dashboard.component";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Dashboard",
      urls: [{ title: "Dashboard", url: "/dashboard" }, { title: "Dashboard" }],
    },
    component: DashboardComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    RouterModule.forChild(routes),
    ChartsModule,
  ],
  declarations: [
    DashboardComponent,
    TickerTapeComponent,
    MarketWatchComponent,
    MarketNewsComponent,
  ],
})
export class DashboardModule {}
