export const scriptTapeText = `{
  symbols: [
    {
      proName: "FOREXCOM:SPXUSD",
      title: "S&P 500",
    },
    {
      proName: "FOREXCOM:NSXUSD",
      title: "Nasdaq 1000",
    },
    {
      proName: "FX_IDC:EURUSD",
      title: "EUR/USD",
    },
    {
      proName: "GEMINI:BTCUSD",
      title: "BTC/USD",
    },
    {
      proName: "GEMINI:ETHUSD",
      title: "ETH/USD",
    },
    {
      "description": "SGD/USD",
      "proName": "FX_IDC:SGDUSD"
    }
  ],
  showSymbolLogo: true,
  colorTheme: "light",
  isTransparent: false,
  displayMode: "adaptive",
  locale: "br",
}`;

export const scriptOverviewText = `{
  "colorTheme": "light",
  "dateRange": "12M",
  "showChart": true,
  "locale": "en",
  "largeChartUrl": "",
  "isTransparent": false,
  "showSymbolLogo": true,
  "showFloatingTooltip": false,
  "width": "500",
  "height": "660",
  "plotLineColorGrowing": "rgba(41, 98, 255, 1)",
  "plotLineColorFalling": "rgba(41, 98, 255, 1)",
  "gridLineColor": "rgba(240, 243, 250, 0)",
  "scaleFontColor": "rgba(120, 123, 134, 1)",
  "belowLineFillColorGrowing": "rgba(41, 98, 255, 0.12)",
  "belowLineFillColorFalling": "rgba(41, 98, 255, 0.12)",
  "belowLineFillColorGrowingBottom": "rgba(41, 98, 255, 0)",
  "belowLineFillColorFallingBottom": "rgba(41, 98, 255, 0)",
  "symbolActiveColor": "rgba(41, 98, 255, 0.12)",
  "tabs": [
    {
      "title": "Indices",
      "symbols": [
        {
          "s": "FOREXCOM:SPXUSD",
          "d": "S&P 500"
        },
        {
          "s": "FOREXCOM:NSXUSD",
          "d": "Nasdaq 100"
        },
        {
          "s": "FOREXCOM:DJI",
          "d": "Dow 30"
        },
        {
          "s": "INDEX:NKY",
          "d": "Nikkei 225"
        },
        {
          "s": "INDEX:DEU30",
          "d": "DAX Index"
        },
        {
          "s": "FOREXCOM:UKXGBP",
          "d": "UK 100"
        }
      ],
      "originalTitle": "Indices"
    },
    {
      "title": "Commodities",
      "symbols": [
        {
          "s": "CME_MINI:ES1!",
          "d": "S&P 500"
        },
        {
          "s": "CME:6E1!",
          "d": "Euro"
        },
        {
          "s": "COMEX:GC1!",
          "d": "Gold"
        },
        {
          "s": "NYMEX:CL1!",
          "d": "Crude Oil"
        },
        {
          "s": "NYMEX:NG1!",
          "d": "Natural Gas"
        },
        {
          "s": "CBOT:ZC1!",
          "d": "Corn"
        }
      ],
      "originalTitle": "Commodities"
    },
    {
      "title": "Bonds",
      "symbols": [
        {
          "s": "CME:GE1!",
          "d": "Eurodollar"
        },
        {
          "s": "CBOT:ZB1!",
          "d": "T-Bond"
        },
        {
          "s": "CBOT:UB1!",
          "d": "Ultra T-Bond"
        },
        {
          "s": "EUREX:FGBL1!",
          "d": "Euro Bund"
        },
        {
          "s": "EUREX:FBTP1!",
          "d": "Euro BTP"
        },
        {
          "s": "EUREX:FGBM1!",
          "d": "Euro BOBL"
        }
      ],
      "originalTitle": "Bonds"
    },
    {
      "title": "Forex",
      "symbols": [
        {
          "s": "FX:EURUSD"
        },
        {
          "s": "FX:GBPUSD"
        },
        {
          "s": "FX:USDJPY"
        },
        {
          "s": "FX:USDCHF"
        },
        {
          "s": "FX:AUDUSD"
        },
        {
          "s": "FX:USDCAD"
        }
      ],
      "originalTitle": "Forex"
    }
  ]
}`;

export const scriptMarketDataText = `{
  "title": "Stocks",
  "width": 1000,
  "height": 660,
  "locale": "en",
  "showSymbolLogo": true,
  "symbolsGroups": [
    {
      "name": "Financial",
      "symbols": [
        {
          "name": "NYSE:JPM",
          "displayName": "Jpmorgan Chase & Co"
        },
        {
          "name": "NYSE:WFC",
          "displayName": "Wells Fargo Co New"
        },
        {
          "name": "NYSE:BAC",
          "displayName": "Bank Amer Corp"
        },
        {
          "name": "NYSE:HSBC",
          "displayName": "Hsbc Hldgs Plc"
        },
        {
          "name": "NYSE:C",
          "displayName": "Citigroup Inc"
        },
        {
          "name": "NYSE:MA",
          "displayName": "Mastercard Incorporated"
        }
      ]
    },
    {
      "name": "Technology",
      "symbols": [
        {
          "name": "NASDAQ:AAPL",
          "displayName": "Apple"
        },
        {
          "name": "NASDAQ:GOOGL",
          "displayName": "Google Inc"
        },
        {
          "name": "NASDAQ:MSFT",
          "displayName": "Microsoft Corp"
        },
        {
          "name": "NASDAQ:FB",
          "displayName": "Facebook Inc"
        },
        {
          "name": "NYSE:ORCL",
          "displayName": "Oracle Corp"
        },
        {
          "name": "NASDAQ:INTC",
          "displayName": "Intel Corp"
        }
      ]
    },
    {
      "name": "Services",
      "symbols": [
        {
          "name": "NASDAQ:AMZN",
          "displayName": "Amazon Com Inc"
        },
        {
          "name": "NYSE:BABA",
          "displayName": "Alibaba Group Hldg Ltd"
        },
        {
          "name": "NYSE:T",
          "displayName": "At&t Inc"
        },
        {
          "name": "NYSE:WMT",
          "displayName": "Wal-mart Stores Inc"
        },
        {
          "name": "NYSE:V",
          "displayName": "Visa Inc"
        }
      ]
    }
  ],
  "colorTheme": "light"
}`;

export const scriptCryptoText = `
{
  "width": 1000,
  "height": 500,
  "defaultColumn": "overview",
  "screener_type": "crypto_mkt",
  "displayCurrency": "USD",
  "colorTheme": "light",
  "locale": "en"
}
`;

export const scriptMarketNewsText = `
{
  "feedMode": "all_symbols",
  "colorTheme": "light",
  "isTransparent": false,
  "displayMode": "regular",
  "width": 500,
  "height": 500,
  "locale": "en"
}
`;
