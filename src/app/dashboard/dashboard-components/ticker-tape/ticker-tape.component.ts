import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from "@angular/core";
import { scriptTapeText } from "../../script-texts";

declare const TradingView: any;

@Component({
  selector: "app-ticker-tape",
  templateUrl: "./ticker-tape.component.html",
  styleUrls: ["./ticker-tape.component.css"],
})
export class TickerTapeComponent implements OnInit {
  constructor(private _renderer2: Renderer2) {}

  @ViewChild("tickerTape") tickerTape?: ElementRef;

  ngOnInit(): void {}

  ngAfterViewInit() {
    let scriptTape = this._renderer2.createElement("script");
    scriptTape.type = `text/javascript`;
    scriptTape.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js";
    scriptTape.async = true;
    scriptTape.text = scriptTapeText;
    this.tickerTape?.nativeElement.appendChild(scriptTape);
  }
}
