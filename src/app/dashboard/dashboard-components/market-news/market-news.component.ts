import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from "@angular/core";
import { scriptCryptoText, scriptMarketNewsText } from "../../script-texts";

@Component({
  selector: "app-market-news",
  templateUrl: "./market-news.component.html",
  styleUrls: ["./market-news.component.css"],
})
export class MarketNewsComponent implements OnInit {
  constructor(private _renderer2: Renderer2) {}

  @ViewChild("crypto") crypto?: ElementRef;
  @ViewChild("marketNews") marketNews?: ElementRef;

  ngOnInit(): void {}

  ngAfterViewInit() {
    let scriptCrypto = this._renderer2.createElement("script");
    scriptCrypto.type = `text/javascript`;
    scriptCrypto.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-screener.js";
    scriptCrypto.async = true;
    scriptCrypto.text = scriptCryptoText;
    this.crypto?.nativeElement.appendChild(scriptCrypto);

    let scriptmarketNews = this._renderer2.createElement("script");
    scriptmarketNews.type = `text/javascript`;
    scriptmarketNews.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-timeline.js";
    scriptmarketNews.async = true;
    scriptmarketNews.text = scriptMarketNewsText;
    this.marketNews?.nativeElement.appendChild(scriptmarketNews);
  }
}
