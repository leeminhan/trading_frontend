import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from "@angular/core";
import { scriptMarketDataText, scriptOverviewText } from "../../script-texts";

@Component({
  selector: "app-market-watch",
  templateUrl: "./market-watch.component.html",
  styleUrls: ["./market-watch.component.css"],
})
export class MarketWatchComponent implements OnInit {
  constructor(private _renderer2: Renderer2) {}

  @ViewChild("overview") overview?: ElementRef;
  @ViewChild("marketData") marketData?: ElementRef;

  ngOnInit(): void {}
  ngAfterViewInit() {
    let scriptOverview = this._renderer2.createElement("script");
    scriptOverview.type = `text/javascript`;
    scriptOverview.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js";
    scriptOverview.async = true;
    scriptOverview.text = scriptOverviewText;
    this.overview?.nativeElement.appendChild(scriptOverview);

    let scriptMarketData = this._renderer2.createElement("script");
    scriptMarketData.type = `text/javascript`;
    scriptMarketData.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-market-quotes.js";
    scriptMarketData.async = true;
    scriptMarketData.text = scriptMarketDataText;
    this.marketData?.nativeElement.appendChild(scriptMarketData);
  }
}
