import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { environment } from "../environments/environment";
import { Portfolio } from "./portfolio";

@Injectable({ providedIn: "root" })
export class PortfolioService {
  private equityUrl = environment.portfolioUrl + "equity";

  constructor(private http: HttpClient) {}

  getPortfolio(): Observable<Array<Portfolio>> {
    const response = this.http
      .get(this.equityUrl)
      .pipe(retry(3), catchError(this.handleError));
    return response as Observable<Array<Portfolio>>;
  }

  getCurrentHoldings(ticker: string) {
    const response = this.http
      .get(this.equityUrl + `/${ticker}`)
      .pipe(retry(3), catchError(this.handleError));
    return response as Observable<Portfolio>;
  }

  getTickerPrice(ticker: string) {
    const response = this.http
      .get(this.equityUrl + `/currPrice/${ticker}`)
      .pipe(retry(3), catchError(this.handleError));
    return response;
  }

  getAllPrice() {
    const response = this.http
      .get(this.equityUrl + "/currPrice/")
      .pipe(retry(3), catchError(this.handleError));
    return response;
  }

  handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `,
        error.error
      );
    }
    // Return an observable with a user-facing error message.
    return throwError("Something bad happened; please try again later.");
  }
}
