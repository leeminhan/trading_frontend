import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { formatter } from "../helper/helper-methods";
import { Portfolio } from "../portfolio";
import { PortfolioService } from "../portfolio.service";
import { TradeDetails } from "./trade-details/trade-details";
import { TradeService } from "./trade-details/trade-details.service";

declare const TradingView: any;

@Component({
  selector: "app-trade",
  templateUrl: "./trade.component.html",
  styleUrls: ["./trade.component.css"],
})
export class TradeComponent implements OnInit {
  @ViewChild("technicalAnalysis") technicalAnalysis?: ElementRef;
  @ViewChild("profile") profile?: ElementRef;

  // Variables

  tradeForm: FormGroup;
  tradeDetails: TradeDetails;
  destroy$: Subject<boolean> = new Subject<boolean>();
  tickerHistory: TradeDetails[] = [];
  currHoldings: Portfolio;
  submitted = false;

  headers = [
    "ID",
    "Created",
    "State",
    "Type",
    "Exchange",
    "Ticker",
    "Quantity",
    "Price (USD)",
  ];

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _renderer2: Renderer2,
    private _tradeService: TradeService,
    private _portfolioService: PortfolioService,
    private _formBuilder: FormBuilder
  ) {
    this.tradeDetails = new TradeDetails("", "", "", "0", 0);
    this.currHoldings = new Portfolio(0, "", 0, "0", "0");
    this.tradeForm = this._formBuilder.group({
      tradeType: ["", Validators.required],
      price: ["", [Validators.required, Validators.min(1)]],
      quantity: ["", [Validators.required, Validators.min(1)]],
    });
  }

  ngOnInit(): void {
    this.tradeDetails.tradeExchange = this._activatedRoute.snapshot.paramMap
      .get("tradeExchange")
      ?.toUpperCase();
    this.tradeDetails.ticker = this._activatedRoute.snapshot.paramMap
      .get("ticker")
      ?.toUpperCase();

    this._tradeService
      .getTradesByTicker(this.tradeDetails.ticker!)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (data: any) => {
          data.map((item: TradeDetails) => {
            item.price = formatter.format(parseFloat(item.price));
          });
          this.tickerHistory = data;
        },
        (err: any) => console.log("getTradesByTicker Error", err)
      );

    this._portfolioService
      .getCurrentHoldings(this.tradeDetails.ticker!)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (data: any) => {
          this.currHoldings.quantity = data.quantity;
          this.currHoldings.avgPrice = formatter.format(data.avgPrice);
          this.currHoldings.costBasis = formatter.format(data.costBasis);
        },
        (err: any) => console.log("getCurrentHoldings Error", err)
      );

    this._portfolioService
      .getTickerPrice(this.tradeDetails.ticker!)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (data: any) => {
          for (let key in data) {
            const defaultPrice: number = data[key].toFixed(2);
            this.tradeForm.patchValue({
              price: defaultPrice,
            });
          }
        },
        (err: any) => console.log("getTickerPrice Error", err)
      );
  }

  ngAfterViewInit(): void {
    new TradingView.widget({
      width: 980,
      height: 610,
      // autosize: true,
      symbol: `${this.tradeDetails.tradeExchange}:${this.tradeDetails.ticker}`,
      timezone: "Etc/UTC",
      theme: "Light",
      style: "1",
      locale: "en",
      toolbar_bg: "#f1f3f6",
      enable_publishing: false,
      withdateranges: true,
      range: "ytd",
      hide_side_toolbar: false,
      allow_symbol_change: true,
      show_popup_button: true,
      popup_width: "1000",
      popup_height: "650",
      no_referral_id: true,
      container_id: "tradingview_bac65",
    });

    let scriptTechnical = this._renderer2.createElement("script");
    scriptTechnical.type = `text/javascript`;
    scriptTechnical.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js";
    scriptTechnical.async = true;
    scriptTechnical.text = `{
      interval: "1m",
      width: 425,
      height: 450,
      symbol: "${this.tradeDetails.tradeExchange}:${this.tradeDetails.ticker}",
      showIntervalTabs: true,
      locale: "en",
      colorTheme: "light",
    }`;
    this.technicalAnalysis?.nativeElement.appendChild(scriptTechnical);
    let scriptProfile = this._renderer2.createElement("script");

    scriptProfile.type = `text/javascript`;
    scriptProfile.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-symbol-profile.js";
    scriptProfile.async = true;
    scriptProfile.text = `
    {
      "symbol": "${this.tradeDetails.tradeExchange}:${this.tradeDetails.ticker}",
      "width": 480,
      "height": 450,
      "colorTheme": "light",
      "isTransparent": false,
      "locale": "en"
    }
    `;
    this.profile?.nativeElement.appendChild(scriptProfile);

    this._portfolioService
      .getTickerPrice(this.tradeDetails.ticker!)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (data: any) => {
          for (let key in data) {
            const defaultPrice: number = data[key].toFixed(2);
            this.tradeForm.patchValue({
              price: defaultPrice,
            });
          }
        },
        (err: any) => console.log("getTickerPrice Error", err)
      );
  }

  onSubmit() {
    this.submitted = true;
    if (this.tradeForm.invalid) return;

    this.tradeDetails.tradeType = this.tradeForm.value.tradeType;
    this.tradeDetails.price = this.tradeForm.value.price;
    this.tradeDetails.quantity = this.tradeForm.value.quantity;
    this._tradeService
      .createTrade(this.tradeDetails)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        console.log(res, "createTradeResponse");
      });
    setTimeout(function () {
      window.location.reload(), 60;
    });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
