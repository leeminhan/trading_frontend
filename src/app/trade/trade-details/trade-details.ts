export class TradeDetails {
  constructor(
    tradeType: string,
    tradeExchange: string,
    ticker: string,
    price: string,
    quantity: number,
    id?: number,
    created?: string,
    tradeState?: string
  ) {
    this.id = id;
    this.created = created;
    this.tradeState = tradeState;
    this.tradeType = tradeType;
    this.tradeExchange = tradeExchange;
    this.ticker = ticker;
    this.price = price;
    this.quantity = quantity;
  }
  id?: number;
  created?: string;
  tradeState?: string;
  tradeType: string;
  tradeExchange?: string;
  ticker?: string;
  quantity: number;
  price: string;
}
