import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { environment } from "../../../environments/environment";
import { TradeDetails } from "./trade-details";

@Injectable({
  providedIn: "root",
})
export class TradeService {
  private tradeUrl = environment.baseUrl + "trades/";

  constructor(private http: HttpClient) {}

  getAllTrades() {
    const response = this.http
      .get(this.tradeUrl)
      .pipe(retry(3), catchError(this.handleError));
    return response as Observable<Array<TradeDetails>>;
  }

  getTradesByState(state: string) {
    const options = { params: new HttpParams().set("tradeState", state) };
    const response = this.http
      .get(this.tradeUrl, options)
      .pipe(retry(3), catchError(this.handleError));
    return response as Observable<Array<TradeDetails>>;
  }

  getTradesByTicker(ticker: string) {
    const options = { params: new HttpParams().set("ticker", ticker) };
    const response = this.http
      .get(this.tradeUrl + "/ticker/", options)
      .pipe(retry(3), catchError(this.handleError));
    return response as Observable<Array<TradeDetails>>;
  }

  createTrade(tradeDetails: TradeDetails) {
    console.log(tradeDetails, "tradeService");
    return this.http
      .post(this.tradeUrl, tradeDetails)
      .pipe(catchError(this.handleError));
  }

  handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `,
        error.error
      );
    }
    // Return an observable with a user-facing error message.
    return throwError("Something bad happened; please try again later.");
  }
}
