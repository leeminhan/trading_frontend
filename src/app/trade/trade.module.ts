import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { ChartsModule } from "ng2-charts";
import { TradeControlComponent } from "./trade-control/trade-control.component";
import { TradeFormComponent } from "./trade-form/trade-form.component";
import { TradeComponent } from "./trade.component";
import { TradingviewComponent } from "./tradingview/tradingview.component";

const routes: Routes = [
  {
    path: "trade",
    data: {
      title: "Trade",
      urls: [
        { title: "Trade", url: "/trade/:exchange/:ticker" },
        { title: "Trade" },
      ],
    },
    component: TradeComponent,
  },
];

@NgModule({
  imports: [
    FormsModule,
    FormControl,
    FormGroup,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
    ChartsModule,
  ],
  declarations: [
    TradeControlComponent,
    TradingviewComponent,
    TradeFormComponent,
  ],
})
export class TradeModule {}
