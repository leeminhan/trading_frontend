import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeControlComponent } from './trade-control.component';

describe('TradeControlComponent', () => {
  let component: TradeControlComponent;
  let fixture: ComponentFixture<TradeControlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradeControlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
