import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";

@Component({
  selector: "app-trade-control",
  templateUrl: "./trade-control.component.html",
  styleUrls: ["./trade-control.component.css"],
})
export class TradeControlComponent {
  tradeForm = new FormGroup({
    price: new FormControl(""),
    quantity: new FormControl(""),
  });

  onSubmit() {
    console.log(this.tradeForm.value);
  }
}
